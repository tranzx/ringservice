package mars.ring.web.rest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.google.gson.Gson;

import mars.ring.RingApp;
import mars.ring.repository.ClockiWorldRecordRepository;
import mars.ring.web.rest.clocki.ClockiWorldRecordCommand;
import mars.ring.web.rest.clocki.ClockiWorldRecordResource;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = RingApp.class)
public class ClockiWorldRecordResourceIntTest {

	@Mock
	private ClockiWorldRecordRepository clockiWorldRecordRepository;

	private MockMvc restClockiMockMvc;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		ClockiWorldRecordResource clockiResource = new ClockiWorldRecordResource(clockiWorldRecordRepository);
		this.restClockiMockMvc = MockMvcBuilders.standaloneSetup(clockiResource).build();
	}

	@Test
	public void getWorldRecords() throws Exception {
		restClockiMockMvc.perform(get("/api/clocki/records"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

	}

	@Test
	public void registerARecord() throws Exception {
		ClockiWorldRecordCommand dto = new ClockiWorldRecordCommand();
		dto.setHero("Frank");
		dto.setLevel1(18);
		restClockiMockMvc.perform(post("/api/clocki/records")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(new Gson().toJson(dto)))
			.andExpect(status().isCreated());

//		restClockiMockMvc.perform(get("/api/clocki/records"))
//			.andExpect(status().isOk())
//			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//			.andExpect(jsonPath("$.level1hero", startsWith("Frank")))
//			.andExpect(jsonPath("$.level1score", is(18)));
	}
}