package mars.ring.application.impl;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mars.ring.application.TrackingEventService;
import mars.ring.domain.TrackingEvent;
import mars.ring.domain.TrackingEventFactory;
import mars.ring.repository.TrackingEventRepository;
import mars.ring.service.dto.RBeaconLTCommand;

@Service
public class TrackingEventServiceImpl implements TrackingEventService {

    private final TrackingEventRepository trackingEventRepository;
    private final TrackingEventFactory trackingEventFactory;

    public TrackingEventServiceImpl(TrackingEventRepository trackingEventRepository,
                                    TrackingEventFactory trackingEventFactory) {
        this.trackingEventRepository = trackingEventRepository;
        this.trackingEventFactory = trackingEventFactory;
    }

    @Override
    @Transactional
    public void registerTrackingEvents(Set<RBeaconLTCommand> traces, String reporter) {
        List<TrackingEvent> events = trackingEventFactory.createTrackingEvents(traces, reporter);
        trackingEventRepository.save(events);
    }

}
