package mars.ring.application.util;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

public class DateUtil {

    public static ZonedDateTime nowUTC() {
        return ZonedDateTime.now(ZoneOffset.UTC);
    }

    public static ZonedDateTime toUTC(Date date) {
        return ZonedDateTime.ofInstant(date.toInstant(), ZoneId.of("UTC").normalized());
    }

}