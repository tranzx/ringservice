package mars.ring.application;

import java.util.Set;

import mars.ring.service.dto.RBeaconLTCommand;

/**
 * Tracking event service.
 */
public interface TrackingEventService {

    /**
     * Registers a tracking event in the system, and notifies interested
     * parties that a beacon has been seen.
     */
    void registerTrackingEvents(Set<RBeaconLTCommand> traces, String usernameOfReporter);

}