package mars.ring.web.rest;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import mars.ring.application.util.DateUtil;
import mars.ring.domain.RBeacon;
import mars.ring.service.RBeaconQueryService;
import mars.ring.service.RBeaconService;
import mars.ring.service.dto.RBeaconCriteria;
import mars.ring.service.dto.RBeaconDTO;
import mars.ring.web.rest.errors.BadRequestAlertException;
import mars.ring.web.rest.util.HeaderUtil;
import mars.ring.web.rest.util.PaginationUtil;

/**
 * REST controller for managing RBeacon.
 */
@RestController
@RequestMapping("/api")
public class RBeaconResource extends BaseAuthenticatedResource {

    private final Logger log = LoggerFactory.getLogger(RBeaconResource.class);

    private static final String ENTITY_NAME = "rBeacon";

    private final RBeaconService rBeaconService;
    private final RBeaconQueryService rBeaconQueryService;

    public RBeaconResource(RBeaconService rBeaconService,
            RBeaconQueryService rBeaconQueryService) {
        this.rBeaconService = rBeaconService;
        this.rBeaconQueryService = rBeaconQueryService;
    }

    /**
     * POST  /r-beacons : Create a new rBeacon.
     *
     * @param rBeacon the rBeacon to create
     * @return the ResponseEntity with status 201 (Created) and with body the new rBeacon,
     * or with status 400 (Bad Request) if the rBeacon has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/r-beacons")
    @Timed
    public ResponseEntity<RBeacon> createRBeacon(@Valid @RequestBody RBeaconDTO dto,
            Principal principal) throws URISyntaxException {
    	RBeacon rBeacon = dto.toModel();
        log.debug("REST request to save RBeacon : {}", rBeacon);
        if (rBeacon.getId() != null) {
            throw new BadRequestAlertException("A new Beacon cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (rBeaconService.isRegisteredBefore(rBeacon)) {
            throw new BadRequestAlertException("This beacon is already registered!", ENTITY_NAME, "macexists");
        }
        rBeacon.setOwner(getCurrentUsername(principal));
        rBeacon.setCreatedAt(DateUtil.nowUTC());
        RBeacon result = rBeaconService.save(rBeacon);
        return ResponseEntity.created(new URI("/api/r-beacons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /r-beacons : Updates an existing rBeacon.
     *
     * @param rBeacon the rBeacon to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated rBeacon,
     * or with status 400 (Bad Request) if the rBeacon is not valid,
     * or with status 500 (Internal Server Error) if the rBeacon couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/r-beacons")
    @Timed
    public ResponseEntity<RBeacon> updateRBeacon(@Valid @RequestBody RBeaconDTO dto,
            Principal principal) throws URISyntaxException {
        log.debug("REST request to update RBeacon : {}", dto);
        if (dto.getId() == null) {
            return ResponseEntity.badRequest().build();
        }
        RBeacon rBeacon = rBeaconService.findOne(dto.getId());
        if (!rBeacon.getOwner().equals(getCurrentUsername(principal))) {
            return ResponseEntity.notFound().build();
        }
        rBeacon.setTagName(dto.getTagName());
        rBeacon.setCategory(dto.getCategory());
        RBeacon result = rBeaconService.save(rBeacon);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, rBeacon.getId().toString()))
            .body(result);
    }

    @PutMapping("/r-beacons/toggleIsMissing/{lost}")
    @Timed
    public ResponseEntity<RBeaconDTO> reportLostBeacon(@Valid @RequestBody RBeaconDTO dto,
                                                       @PathVariable("lost") Boolean lost,
                                                       Principal principal) {
        if (dto.getId() == null) {
            return ResponseEntity.badRequest().build();
        }
        RBeacon aTag = rBeaconService.findOne(dto.getId());
        if (aTag == null || !aTag.getOwner().equals(getCurrentUsername(principal))) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        aTag.setLost(lost); // toggle isMissing
        rBeaconService.save(aTag);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dto.getId().toString()))
                .body(dto);
    }

    /**
     * GET  /r-beacons : get all the rBeacons.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of rBeacons in body
     */
    @GetMapping("/r-beacons")
    @Timed
    public ResponseEntity<List<RBeaconDTO>> getAllRBeacons(RBeaconCriteria criteria,
            Pageable pageable, Principal principal) {
        criteria.setOwner(getCurrentUsernameFilter(principal));
        log.debug("REST request to get RBeacons by criteria: {}", criteria);
        if (pageable.getSort() == null || !pageable.getSort().iterator().hasNext()) {   // Default sort rBeacon.created DESC
            pageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort(Direction.DESC, "created"));
        }
        Page<RBeacon> page = rBeaconQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/r-beacons");
        return new ResponseEntity<>(RBeaconDTO.toDTOList(page.getContent()), headers, HttpStatus.OK);
    }

	/**
     * GET  /r-beacons/:id : get the "id" rBeacon.
     *
     * @param id the id of the rBeacon to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the rBeacon, or with status 404 (Not Found)
     */
    @GetMapping("/r-beacons/{id}")
    @Timed
    public ResponseEntity<RBeaconDTO> getRBeacon(@PathVariable Long id, Principal principal) {
        log.debug("REST request to get RBeacon : {}", id);
        RBeacon rBeacon = rBeaconService.findOne(id);
        if (rBeacon != null && !rBeacon.getOwner().equals(getCurrentUsername(principal))) {
            return ResponseEntity.notFound().build();
        }
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(rBeacon == null ? null : new RBeaconDTO(rBeacon)));
    }

    /**
     * DELETE  /r-beacons/:id : delete the "id" rBeacon.
     *
     * @param id the id of the rBeacon to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/r-beacons/{id}")
    @Timed
    public ResponseEntity<Void> deleteRBeacon(@PathVariable Long id, Principal principal) {
        log.debug("REST request to delete RBeacon : {}", id);
        RBeacon rBeacon = rBeaconService.findOne(id);
        if (!rBeacon.getOwner().equals(getCurrentUsername(principal))) {
            return ResponseEntity.notFound().build();
        }
        rBeaconService.delete(id);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString()))
                .build();
    }

}
