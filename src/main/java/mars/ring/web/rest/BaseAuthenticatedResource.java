package mars.ring.web.rest;

import java.security.Principal;
import java.util.Optional;

import org.springframework.security.oauth2.provider.OAuth2Authentication;

import io.github.jhipster.service.filter.StringFilter;
import mars.ring.web.rest.errors.InternalServerErrorException;

public class BaseAuthenticatedResource {

	protected String getCurrentUsername(Principal principal) {
		return Optional.ofNullable(principal)
				.filter(it -> it instanceof OAuth2Authentication)
				.map(it -> ((OAuth2Authentication) it).getUserAuthentication())
				.map(authentication -> {
					return authentication.getName();
				})
				.orElseThrow(() -> new InternalServerErrorException("User could not be found"));
	}

	protected String getCurrentUsernameOptional(Principal principal) {
		return Optional.ofNullable(principal)
				.filter(it -> it instanceof OAuth2Authentication)
				.map(it -> ((OAuth2Authentication) it).getUserAuthentication())
				.map(authentication -> {
					return authentication.getName();
				})
				.orElse(null);
	}

	protected StringFilter getCurrentUsernameFilter(Principal principal) {
		StringFilter filter = new StringFilter();
		filter.setEquals(getCurrentUsername(principal));
		return filter;
	}

}