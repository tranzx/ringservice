package mars.ring.web.rest.easypublish;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mars.ring.domain.easypublish.Box;
import mars.ring.domain.easypublish.BoxVersion;
import mars.ring.domain.easypublish.PackageType;

public class BoxCommand {

    @NotNull
    @Size(min = 3)
    private String packageId;
    @NotNull
    @Size(min = 3)
    private PackageType packageType;
    @NotNull
    @Size(min = 1)
    private String version;
    @NotNull
    @Size(min = 10)
    private String downloadUrl;

    public Box toBox(String login) {
        Box box = new Box();
        box.setLogin(login);
        box.setPackageId(getPackageId());
        box.setPackageType(getPackageType());
        return box;
    }

    public BoxVersion toBoxVersion() {
        BoxVersion bv = new BoxVersion();
        bv.setVersion(getVersion());
        bv.setDownloadUrl(getDownloadUrl());
        bv.setCreation(ZonedDateTime.now(ZoneOffset.UTC));
        return bv;
    }

    public String getPackageId() {
        return packageId;
    }

    public PackageType getPackageType() {
        return packageType;
    }

    public String getVersion() {
        return version;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

}