package mars.ring.web.rest.easypublish;

import com.codahale.metrics.annotation.Timed;

import mars.ring.domain.easypublish.Box;
import mars.ring.domain.easypublish.BoxVersion;
import mars.ring.repository.BoxRepository;
import mars.ring.repository.BoxVersionRepository;
import mars.ring.web.rest.BaseAuthenticatedResource;
import mars.ring.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.List;

/**
 * REST controller for managing Box.
 */
@RestController
@RequestMapping("/api/ezp")
public class BoxResource extends BaseAuthenticatedResource {

    private final Logger log = LoggerFactory.getLogger(BoxResource.class);

    private static final String ENTITY_NAME = "box";

    private final BoxRepository boxRepository;
    private final BoxVersionRepository boxVersionRepository;

    public BoxResource(BoxRepository boxRepository, BoxVersionRepository boxVersionRepository) {
        this.boxRepository = boxRepository;
        this.boxVersionRepository = boxVersionRepository;
    }

    /**
     * POST  /boxes : Create a new box.
     *
     * @param box the box to create
     * @return the ResponseEntity with status 201 (Created) and with body the new box, or with status 400 (Bad Request) if the box has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping(value = "/boxes", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public ResponseEntity<String> createBox(@Valid @RequestBody BoxCommand cmd, Principal principal) throws URISyntaxException {
        log.debug("REST request to save Box : {}", cmd);
        Box box = boxRepository.findOneByPackageIdAndLogin(cmd.getPackageId(), getCurrentUsername(principal));
        boolean isNew = false;
        if (box == null) {
            isNew = true;
        } else {
            BoxVersion boxVersion = boxVersionRepository.findOneByBoxAndVersion(box, cmd.getVersion());
            if (boxVersion == null) {
                isNew = true;
            }
        }
        if (isNew) {
            if (box == null) {
                box = boxRepository.save(cmd.toBox(getCurrentUsername(principal)));
            }
            BoxVersion boxVersion = cmd.toBoxVersion();
            boxVersion.setBox(box);
            boxVersionRepository.save(boxVersion);
            return ResponseEntity.created(new URI("/api/ezp/boxes/" + cmd.getPackageId()))
                    .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, cmd.getPackageId()))
                    .body(cmd.getPackageId());
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    /**
     * GET  /boxes : get all the boxes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of boxes in body
     */
    @GetMapping("/boxes")
    @Timed
    public List<Box> getAllBoxes(Principal principal) {
        log.debug("REST request to get all Boxes");
        return boxRepository.findByLogin(getCurrentUsername(principal));
    }

    /**
     * DELETE  /boxes/:packageId : delete the box and related versions.
     *
     * @param packageId the packageId of the box to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/boxes/{packageId}")
    @Timed
    @Transactional
    public ResponseEntity<Void> deleteBox(@PathVariable String packageId, Principal principal) {
        log.debug("REST request to delete Box : {}", packageId);
        Box box = boxRepository.findOneByPackageIdAndLogin(packageId, getCurrentUsername(principal));
        boxVersionRepository.deleteByBox(box);
        boxRepository.delete(box);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, packageId)).build();
    }

}
