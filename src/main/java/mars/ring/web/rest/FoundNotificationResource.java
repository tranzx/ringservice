package mars.ring.web.rest;

import java.security.Principal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import mars.ring.domain.FoundNotification;
import mars.ring.service.FoundNotificationQueryService;
import mars.ring.service.dto.FoundNotificationCriteria;
import mars.ring.service.dto.FoundNotificationDTO;
import mars.ring.web.rest.util.PaginationUtil;

/**
 * REST controller for managing FoundNotification.
 */
@RestController
@RequestMapping("/api")
public class FoundNotificationResource extends BaseAuthenticatedResource {

    private final Logger log = LoggerFactory.getLogger(FoundNotificationResource.class);

    private final FoundNotificationQueryService foundNotificationQueryService;

    public FoundNotificationResource(FoundNotificationQueryService foundNotificationQueryService) {
        this.foundNotificationQueryService = foundNotificationQueryService;
    }

    /**
     * GET  /found-notifications : get all the foundNotifications.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of foundNotifications in body
     */
    @GetMapping("/found-notifications")
    @Timed
    public ResponseEntity<List<FoundNotificationDTO>> getAllFoundNotifications(
            FoundNotificationCriteria criteria,
            Pageable pageable, Principal principal) {
        criteria.setOwner(getCurrentUsernameFilter(principal));
        log.debug("REST request to get FoundNotifications by criteria: {}", criteria);
        Page<FoundNotification> page = foundNotificationQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/found-notifications");
        return new ResponseEntity<>(FoundNotificationDTO.toDTO(page.getContent()), headers, HttpStatus.OK);
    }

}
