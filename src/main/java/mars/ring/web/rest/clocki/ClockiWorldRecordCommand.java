package mars.ring.web.rest.clocki;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import mars.ring.domain.clocki.ClockiWorldRecord;

public class ClockiWorldRecordCommand {

    String hero;

    Integer level1;
    Integer level2;
    Integer level3;
    Integer level4;
    Integer level5;
    Integer level6;
    Integer level7;
    Integer level8;
    Integer level9;

    public ClockiWorldRecord toEntity(ClockiWorldRecord currentRecord) {
        if (currentRecord == null) {
            currentRecord = new ClockiWorldRecord();
        }
        setLevelsFor(currentRecord);
        currentRecord.setLastUpdateBy(hero);
        currentRecord.setLastUpdateTime(ZonedDateTime.now(ZoneOffset.UTC));
        return currentRecord;
    }

    public void setLevelsFor(ClockiWorldRecord current) {
        if (level1 != null) {
            if (current.getLevel1() == null || current.getLevel1() >= level1) {
                current.setLevel1(level1);
                current.setLevel1hero(hero);
            }
        }
        if (level2 != null) {
            if (current.getLevel2() == null || current.getLevel2() >= level2) {
                current.setLevel2(level2);
                current.setLevel2hero(hero);
            }
        }
        if (level3 != null) {
            if (current.getLevel3() == null || current.getLevel3() >= level3) {
                current.setLevel3(level3);
                current.setLevel3hero(hero);
            }
        }
        if (level4 != null) {
            if (current.getLevel4() == null || current.getLevel4() >= level4) {
                current.setLevel4(level4);
                current.setLevel4hero(hero);
            }
        }
        if (level5 != null) {
            if (current.getLevel5() == null || current.getLevel5() >= level5) {
                current.setLevel5(level5);
                current.setLevel5hero(hero);
            }
        }
        if (level6 != null) {
            if (current.getLevel6() == null || current.getLevel6() >= level6) {
                current.setLevel6(level6);
                current.setLevel6hero(hero);
            }
        }
        if (level7 != null) {
            if (current.getLevel7() == null || current.getLevel7() >= level7) {
                current.setLevel7(level7);
                current.setLevel7hero(hero);
            }
        }
        if (level8 != null) {
            if (current.getLevel8() == null || current.getLevel8() >= level8) {
                current.setLevel8(level8);
                current.setLevel8hero(hero);
            }
        }
        if (level9 != null) {
            if (current.getLevel9() == null || current.getLevel9() >= level9) {
                current.setLevel9(level9);
                current.setLevel9hero(hero);
            }
        }
    }

    public String getHero() {
        return hero;
    }
    public void setHero(String hero) {
        this.hero = hero;
    }
    public Integer getLevel1() {
        return level1;
    }
    public void setLevel1(Integer level1) {
        this.level1 = level1;
    }
    public Integer getLevel2() {
        return level2;
    }
    public void setLevel2(Integer level2) {
        this.level2 = level2;
    }
    public Integer getLevel3() {
        return level3;
    }
    public void setLevel3(Integer level3) {
        this.level3 = level3;
    }
    public Integer getLevel4() {
        return level4;
    }
    public void setLevel4(Integer level4) {
        this.level4 = level4;
    }
    public Integer getLevel5() {
        return level5;
    }
    public void setLevel5(Integer level5) {
        this.level5 = level5;
    }
    public Integer getLevel6() {
        return level6;
    }
    public void setLevel6(Integer level6) {
        this.level6 = level6;
    }
    public Integer getLevel7() {
        return level7;
    }
    public void setLevel7(Integer level7) {
        this.level7 = level7;
    }
    public Integer getLevel8() {
        return level8;
    }
    public void setLevel8(Integer level8) {
        this.level8 = level8;
    }
    public Integer getLevel9() {
        return level9;
    }
    public void setLevel9(Integer level9) {
        this.level9 = level9;
    }

}