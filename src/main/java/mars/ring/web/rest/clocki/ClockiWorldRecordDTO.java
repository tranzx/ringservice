package mars.ring.web.rest.clocki;

import mars.ring.domain.clocki.ClockiWorldRecord;

/**
 * @author developer
 *
 */
public class ClockiWorldRecordDTO {

    Integer level1;
    Integer level2;
    Integer level3;
    Integer level4;
    Integer level5;
    Integer level6;
    Integer level7;
    Integer level8;
    Integer level9;

    String level1hero;
    String level2hero;
    String level3hero;
    String level4hero;
    String level5hero;
    String level6hero;
    String level7hero;
    String level8hero;
    String level9hero;

    public ClockiWorldRecordDTO() {}

    public ClockiWorldRecordDTO(ClockiWorldRecord entity) {
    	if (entity != null) {
	        level1 = entity.getLevel1();
	        level2 = entity.getLevel2();
	        level3 = entity.getLevel3();
	        level4 = entity.getLevel4();
	        level5 = entity.getLevel5();
	        level6 = entity.getLevel6();
	        level7 = entity.getLevel7();
	        level8 = entity.getLevel8();
	        level9 = entity.getLevel9();

	        level1hero = entity.getLevel1hero();
	        level2hero = entity.getLevel2hero();
	        level3hero = entity.getLevel3hero();
	        level4hero = entity.getLevel4hero();
	        level5hero = entity.getLevel5hero();
	        level6hero = entity.getLevel6hero();
	        level7hero = entity.getLevel7hero();
	        level8hero = entity.getLevel8hero();
	        level9hero = entity.getLevel9hero();
    	}
    }

    public Integer getLevel1() {
        return level1;
    }

    public Integer getLevel2() {
        return level2;
    }

    public Integer getLevel3() {
        return level3;
    }

    public Integer getLevel4() {
        return level4;
    }

    public Integer getLevel5() {
        return level5;
    }

    public Integer getLevel6() {
        return level6;
    }

    public Integer getLevel7() {
        return level7;
    }

    public Integer getLevel8() {
        return level8;
    }

    public Integer getLevel9() {
        return level9;
    }

    public String getLevel1hero() {
        return level1hero;
    }

    public String getLevel2hero() {
        return level2hero;
    }

    public String getLevel3hero() {
        return level3hero;
    }

    public String getLevel4hero() {
        return level4hero;
    }

    public String getLevel5hero() {
        return level5hero;
    }

    public String getLevel6hero() {
        return level6hero;
    }

    public String getLevel7hero() {
        return level7hero;
    }

    public String getLevel8hero() {
        return level8hero;
    }

    public String getLevel9hero() {
        return level9hero;
    }

}