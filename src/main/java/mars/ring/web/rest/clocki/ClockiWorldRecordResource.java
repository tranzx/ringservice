package mars.ring.web.rest.clocki;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import mars.ring.domain.clocki.ClockiWorldRecord;
import mars.ring.repository.ClockiWorldRecordRepository;
import mars.ring.web.rest.util.HeaderUtil;

/**
 * REST controller for managing ClockiWorldRecord.
 */
@RestController
@RequestMapping("/api/clocki")
public class ClockiWorldRecordResource {

    private final Logger log = LoggerFactory.getLogger(ClockiWorldRecordResource.class);

    private static final String ENTITY_NAME = "clockiWorldRecord";
    private static final String ROUND_NAME = "round1ever";  // Note: changing this value will reset world records to zero/null!

    private final ClockiWorldRecordRepository clockiWorldRecordRepository;

    public ClockiWorldRecordResource(ClockiWorldRecordRepository clockiWorldRecordRepository) {
        this.clockiWorldRecordRepository = clockiWorldRecordRepository;
    }

    @PostMapping("/records")
    @Timed
    public ResponseEntity<ClockiWorldRecordDTO> create(@Valid @RequestBody ClockiWorldRecordCommand cmd) throws URISyntaxException {
        log.info("REST request to save ClockiWorldRecord : {}", cmd);
        ClockiWorldRecord aRecordForSave = currentRound();
        aRecordForSave = cmd.toEntity(aRecordForSave);
        aRecordForSave.setRoundName(ROUND_NAME);

        ClockiWorldRecord result = clockiWorldRecordRepository.save(aRecordForSave);
        return ResponseEntity.created(new URI("/api/clocki/records"))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, ROUND_NAME))
            .body(new ClockiWorldRecordDTO(result));
    }

    @GetMapping("/records")
    @Timed
    public ResponseEntity<ClockiWorldRecordDTO> get() {
        log.info("REST request to get ClockiWorldRecord for round : {}", ROUND_NAME);
        ClockiWorldRecordDTO dto = new ClockiWorldRecordDTO();
        ClockiWorldRecord current = currentRound();
        if (current != null) {
            dto = new ClockiWorldRecordDTO(current);
        }
        return ResponseEntity.ok(dto);
    }

    private ClockiWorldRecord currentRound() {
        List<ClockiWorldRecord> list = clockiWorldRecordRepository.findByRoundName(ROUND_NAME);
        if (list != null && list.size() > 0) {
            return list.get(0);
        }
        return new ClockiWorldRecord();
    }

}