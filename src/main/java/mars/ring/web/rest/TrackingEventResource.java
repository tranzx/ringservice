package mars.ring.web.rest;

import java.security.Principal;
import java.util.List;
import java.util.Set;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import mars.ring.application.TrackingEventService;
import mars.ring.domain.TrackingEvent;
import mars.ring.repository.TrackingEventRepository;
import mars.ring.service.dto.RBeaconLTCommand;
import mars.ring.service.dto.RBeaconLTDTO;

@RestController
@RequestMapping("/api/ble")
public class TrackingEventResource extends BaseAuthenticatedResource {

    private final TrackingEventService trackingEventService;
    private final TrackingEventRepository trackingEventRepository;

    public TrackingEventResource(TrackingEventService trackingEventService,
    		                     TrackingEventRepository trackingEventRepository) {
        this.trackingEventService = trackingEventService;
        this.trackingEventRepository = trackingEventRepository;
    }

    @PostMapping("/trace")
    @Timed
    public ResponseEntity<Void> createEvents(@RequestBody Set<RBeaconLTCommand> traces, Principal principal) {
        trackingEventService.registerTrackingEvents(traces, getCurrentUsernameOptional(principal));
        return ResponseEntity.ok().build();
    }

    @GetMapping("/locations")
    @Timed
    public ResponseEntity<List<RBeaconLTDTO>> getLatest(Principal principal) {
        List<TrackingEvent> list = trackingEventRepository.findByOwner(getCurrentUsername(principal));
        return ResponseEntity.ok(RBeaconLTDTO.fromModel(list));
    }

}