/**
 * View Models used by Spring MVC REST controllers.
 */
package mars.ring.web.rest.vm;
