package mars.ring.repository;

import org.springframework.stereotype.Repository;

import mars.ring.domain.easypublish.Box;

import java.util.List;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Box entity.
 */
@Repository
public interface BoxRepository extends JpaRepository<Box, Long> {

    Box findOneByPackageIdAndLogin(String packageId, String login);

    List<Box> findByLogin(String currentUsername);

}
