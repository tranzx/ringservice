package mars.ring.repository;

import mars.ring.domain.RBeacon;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the RBeacon entity.
 */
@Repository
public interface RBeaconRepository extends JpaRepository<RBeacon, Long>, JpaSpecificationExecutor<RBeacon> {

    RBeacon findByMac(String macAddress);

}
