package mars.ring.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mars.ring.domain.RBeacon;
import mars.ring.domain.TrackingEvent;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the TrackingEvent entity.
 */
@Repository
public interface TrackingEventRepository extends JpaRepository<TrackingEvent, Long> {

    List<TrackingEvent> findByOwner(String owner);

    @Transactional
    List<TrackingEvent> deleteByMac(String mac);

    @Transactional
    List<TrackingEvent> deleteByRBeacon(RBeacon rBeacon);
}
