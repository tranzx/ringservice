package mars.ring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mars.ring.domain.clocki.ClockiWorldRecord;


/**
 * Spring Data JPA repository for the ClockiWorldRecord entity.
 */
@Repository
public interface ClockiWorldRecordRepository extends JpaRepository<ClockiWorldRecord, Long> {

    List<ClockiWorldRecord> findByRoundName(String roundName);

}