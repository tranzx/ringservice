package mars.ring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mars.ring.domain.FoundNotification;
import mars.ring.domain.RBeacon;

/**
 * Spring Data JPA repository for the FoundNotification entity.
 */
@Repository
public interface FoundNotificationRepository extends JpaRepository<FoundNotification, Long>,
                                                     JpaSpecificationExecutor<FoundNotification> {

    @Transactional
    List<FoundNotification> deleteByRBeacon(RBeacon rBeacon);

}