package mars.ring.repository;

import org.springframework.stereotype.Repository;

import mars.ring.domain.easypublish.Box;
import mars.ring.domain.easypublish.BoxVersion;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the BoxVersion entity.
 */
@Repository
public interface BoxVersionRepository extends JpaRepository<BoxVersion, Long> {

    BoxVersion findOneByBoxAndVersion(Box box, String version);

    void deleteByBox(Box box);

}
