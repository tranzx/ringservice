package mars.ring.domain.clocki;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ClockiWorldRecord.
 */
@Entity
@Table(name = "clocki_world_record")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClockiWorldRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "last_update_by")
    private String lastUpdateBy;

    @Column(name = "last_update_time")
    private ZonedDateTime lastUpdateTime;

    @Column(name = "level_1")
    private Integer level1;

    @Column(name = "level_1_hero")
    private String level1hero;

    @Column(name = "level_2")
    private Integer level2;

    @Column(name = "level_2_hero")
    private String level2hero;

    @Column(name = "level_3")
    private Integer level3;

    @Column(name = "level_3_hero")
    private String level3hero;

    @Column(name = "level_4")
    private Integer level4;

    @Column(name = "level_4_hero")
    private String level4hero;

    @Column(name = "level_5")
    private Integer level5;

    @Column(name = "level_5_hero")
    private String level5hero;

    @Column(name = "level_6")
    private Integer level6;

    @Column(name = "level_6_hero")
    private String level6hero;

    @Column(name = "level_7")
    private Integer level7;

    @Column(name = "level_7_hero")
    private String level7hero;

    @Column(name = "level_8")
    private Integer level8;

    @Column(name = "level_8_hero")
    private String level8hero;

    @Column(name = "level_9")
    private Integer level9;

    @Column(name = "level_9_hero")
    private String level9hero;

    @NotNull
    @Column(name = "round_name", nullable = false)
    private String roundName;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    public ClockiWorldRecord lastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
        return this;
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }

    public ZonedDateTime getLastUpdateTime() {
        return lastUpdateTime;
    }

    public ClockiWorldRecord lastUpdateTime(ZonedDateTime lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
        return this;
    }

    public void setLastUpdateTime(ZonedDateTime lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public Integer getLevel1() {
        return level1;
    }

    public ClockiWorldRecord level1(Integer level1) {
        this.level1 = level1;
        return this;
    }

    public void setLevel1(Integer level1) {
        this.level1 = level1;
    }

    public String getLevel1hero() {
        return level1hero;
    }

    public ClockiWorldRecord level1hero(String level1hero) {
        this.level1hero = level1hero;
        return this;
    }

    public void setLevel1hero(String level1hero) {
        this.level1hero = level1hero;
    }

    public Integer getLevel2() {
        return level2;
    }

    public ClockiWorldRecord level2(Integer level2) {
        this.level2 = level2;
        return this;
    }

    public void setLevel2(Integer level2) {
        this.level2 = level2;
    }

    public String getLevel2hero() {
        return level2hero;
    }

    public ClockiWorldRecord level2hero(String level2hero) {
        this.level2hero = level2hero;
        return this;
    }

    public void setLevel2hero(String level2hero) {
        this.level2hero = level2hero;
    }

    public Integer getLevel3() {
        return level3;
    }

    public ClockiWorldRecord level3(Integer level3) {
        this.level3 = level3;
        return this;
    }

    public void setLevel3(Integer level3) {
        this.level3 = level3;
    }

    public String getLevel3hero() {
        return level3hero;
    }

    public ClockiWorldRecord level3hero(String level3hero) {
        this.level3hero = level3hero;
        return this;
    }

    public void setLevel3hero(String level3hero) {
        this.level3hero = level3hero;
    }

    public Integer getLevel4() {
        return level4;
    }

    public ClockiWorldRecord level4(Integer level4) {
        this.level4 = level4;
        return this;
    }

    public void setLevel4(Integer level4) {
        this.level4 = level4;
    }

    public String getLevel4hero() {
        return level4hero;
    }

    public ClockiWorldRecord level4hero(String level4hero) {
        this.level4hero = level4hero;
        return this;
    }

    public void setLevel4hero(String level4hero) {
        this.level4hero = level4hero;
    }

    public Integer getLevel5() {
        return level5;
    }

    public ClockiWorldRecord level5(Integer level5) {
        this.level5 = level5;
        return this;
    }

    public void setLevel5(Integer level5) {
        this.level5 = level5;
    }

    public String getLevel5hero() {
        return level5hero;
    }

    public ClockiWorldRecord level5hero(String level5hero) {
        this.level5hero = level5hero;
        return this;
    }

    public void setLevel5hero(String level5hero) {
        this.level5hero = level5hero;
    }

    public Integer getLevel6() {
        return level6;
    }

    public ClockiWorldRecord level6(Integer level6) {
        this.level6 = level6;
        return this;
    }

    public void setLevel6(Integer level6) {
        this.level6 = level6;
    }

    public String getLevel6hero() {
        return level6hero;
    }

    public ClockiWorldRecord level6hero(String level6hero) {
        this.level6hero = level6hero;
        return this;
    }

    public void setLevel6hero(String level6hero) {
        this.level6hero = level6hero;
    }

    public Integer getLevel7() {
        return level7;
    }

    public ClockiWorldRecord level7(Integer level7) {
        this.level7 = level7;
        return this;
    }

    public void setLevel7(Integer level7) {
        this.level7 = level7;
    }

    public String getLevel7hero() {
        return level7hero;
    }

    public ClockiWorldRecord level7hero(String level7hero) {
        this.level7hero = level7hero;
        return this;
    }

    public void setLevel7hero(String level7hero) {
        this.level7hero = level7hero;
    }

    public Integer getLevel8() {
        return level8;
    }

    public ClockiWorldRecord level8(Integer level8) {
        this.level8 = level8;
        return this;
    }

    public void setLevel8(Integer level8) {
        this.level8 = level8;
    }

    public String getLevel8hero() {
        return level8hero;
    }

    public ClockiWorldRecord level8hero(String level8hero) {
        this.level8hero = level8hero;
        return this;
    }

    public void setLevel8hero(String level8hero) {
        this.level8hero = level8hero;
    }

    public Integer getLevel9() {
        return level9;
    }

    public ClockiWorldRecord level9(Integer level9) {
        this.level9 = level9;
        return this;
    }

    public void setLevel9(Integer level9) {
        this.level9 = level9;
    }

    public String getLevel9hero() {
        return level9hero;
    }

    public ClockiWorldRecord level9hero(String level9hero) {
        this.level9hero = level9hero;
        return this;
    }

    public void setLevel9hero(String level9hero) {
        this.level9hero = level9hero;
    }

    public String getRoundName() {
        return roundName;
    }

    public ClockiWorldRecord roundName(String roundName) {
        this.roundName = roundName;
        return this;
    }

    public void setRoundName(String roundName) {
        this.roundName = roundName;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClockiWorldRecord clockiWorldRecord = (ClockiWorldRecord) o;
        if (clockiWorldRecord.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clockiWorldRecord.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClockiWorldRecord{" +
            "id=" + getId() +
            ", lastUpdateBy='" + getLastUpdateBy() + "'" +
            ", lastUpdateTime='" + getLastUpdateTime() + "'" +
            ", level1=" + getLevel1() +
            ", level1hero='" + getLevel1hero() + "'" +
            ", level2=" + getLevel2() +
            ", level2hero='" + getLevel2hero() + "'" +
            ", level3=" + getLevel3() +
            ", level3hero='" + getLevel3hero() + "'" +
            ", level4=" + getLevel4() +
            ", level4hero='" + getLevel4hero() + "'" +
            ", level5=" + getLevel5() +
            ", level5hero='" + getLevel5hero() + "'" +
            ", level6=" + getLevel6() +
            ", level6hero='" + getLevel6hero() + "'" +
            ", level7=" + getLevel7() +
            ", level7hero='" + getLevel7hero() + "'" +
            ", level8=" + getLevel8() +
            ", level8hero='" + getLevel8hero() + "'" +
            ", level9=" + getLevel9() +
            ", level9hero='" + getLevel9hero() + "'" +
            ", roundName='" + getRoundName() + "'" +
            "}";
    }

}