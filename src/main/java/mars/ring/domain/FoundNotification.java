package mars.ring.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import mars.ring.application.util.DateUtil;

/**
 * A FoundNotification.
 */
@Entity
@Table(name = "found_notification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class FoundNotification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "owner", nullable = false)
    private String owner;

    @Column(name = "lat")
    private Double lat;

    @Column(name = "lon")
    private Double lon;

    @Column(name = "distance")
    private Double distance;

    @Column(name = "recorded_at")
    private ZonedDateTime recordedAt;

    @Column(name = "received_at")
    private ZonedDateTime receivedAt;

    @NotNull
    @Column(name = "created_at", nullable = false)
    private ZonedDateTime createdAt;

    @ManyToOne(optional = false)
    @NotNull
    private RBeacon rBeacon;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public FoundNotification() { }

    public FoundNotification(TrackingEvent event) {
        this.owner = event.getOwner();
        this.rBeacon = event.getRBeacon();
        this.lat = event.getLatitude();
        this.lon = event.getLongitude();
        this.distance = event.getDistance();
        this.recordedAt = event.getRecordTime();
        this.receivedAt = event.getReceivedTime();
        this.createdAt = DateUtil.nowUTC();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public FoundNotification owner(String owner) {
        this.owner = owner;
        return this;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Double getLat() {
        return lat;
    }

    public FoundNotification lat(Double lat) {
        this.lat = lat;
        return this;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public FoundNotification lon(Double lon) {
        this.lon = lon;
        return this;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getDistance() {
        return distance;
    }

    public FoundNotification distance(Double distance) {
        this.distance = distance;
        return this;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public ZonedDateTime getRecordedAt() {
        return recordedAt;
    }

    public FoundNotification recordedAt(ZonedDateTime recordedAt) {
        this.recordedAt = recordedAt;
        return this;
    }

    public void setRecordedAt(ZonedDateTime recordedAt) {
        this.recordedAt = recordedAt;
    }

    public ZonedDateTime getReceivedAt() {
        return receivedAt;
    }

    public FoundNotification receivedAt(ZonedDateTime receivedAt) {
        this.receivedAt = receivedAt;
        return this;
    }

    public void setReceivedAt(ZonedDateTime receivedAt) {
        this.receivedAt = receivedAt;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public FoundNotification createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public RBeacon getRBeacon() {
        return rBeacon;
    }

    public FoundNotification rBeacon(RBeacon rBeacon) {
        this.rBeacon = rBeacon;
        return this;
    }

    public void setRBeacon(RBeacon rBeacon) {
        this.rBeacon = rBeacon;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FoundNotification foundNotification = (FoundNotification) o;
        if (foundNotification.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), foundNotification.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FoundNotification{" +
            "id=" + getId() +
            ", owner='" + getOwner() + "'" +
            ", lat=" + getLat() +
            ", lon=" + getLon() +
            ", distance=" + getDistance() +
            ", recordedAt='" + getRecordedAt() + "'" +
            ", receivedAt='" + getReceivedAt() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            "}";
    }
}
