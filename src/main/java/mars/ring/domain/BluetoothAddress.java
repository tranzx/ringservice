package mars.ring.domain;

import org.apache.commons.lang3.Validate;

import mars.ring.domain.shared.ValueObject;

public final class BluetoothAddress implements ValueObject<BluetoothAddress> {

    private String mac;

    public BluetoothAddress(final String mac) {
        Validate.notNull(mac);
        this.mac = mac;
    }

    public String idString() {
        return mac;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BluetoothAddress other = (BluetoothAddress) o;
        return sameValueAs(other);
    }

    @Override
    public boolean sameValueAs(BluetoothAddress other) {
        return other != null && this.mac.equals(other.mac);
    }

    @Override
    public int hashCode() {
        return mac.hashCode();
    }

    @Override
    public String toString() {
        return mac;
    }

    private static final long serialVersionUID = 1L;

}