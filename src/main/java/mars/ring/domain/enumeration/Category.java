package mars.ring.domain.enumeration;

/**
 * The Category enumeration.
 */
public enum Category {
    KEYS, WALLET, BAG, ACCESSORIES
}
