package mars.ring.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import mars.ring.domain.enumeration.Category;

/**
 * A RBeacon.
 */
@Entity
@Table(name = "rbeacon")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RBeacon implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "owner", nullable = false)
    private String owner;

    @NotNull
    @Column(name = "tag_name", nullable = false)
    private String tagName;

    @NotNull
    @Column(name = "identifier", nullable = false)
    private String identifier;

    @NotNull
    @Column(name = "major", nullable = false)
    private Integer major;

    @NotNull
    @Column(name = "minor", nullable = false)
    private Integer minor;

    @NotNull
    @Column(name = "mac", nullable = false)
    private String mac;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "category")
    private Category category;

    @Column(name = "lost")
    private Boolean lost;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public boolean isMissing() {
        return lost != null && lost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public RBeacon owner(String owner) {
        this.owner = owner;
        return this;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTagName() {
        return tagName;
    }

    public RBeacon tagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getIdentifier() {
        return identifier;
    }

    public RBeacon identifier(String identifier) {
        this.identifier = identifier;
        return this;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Integer getMajor() {
        return major;
    }

    public RBeacon major(Integer major) {
        this.major = major;
        return this;
    }

    public void setMajor(Integer major) {
        this.major = major;
    }

    public Integer getMinor() {
        return minor;
    }

    public RBeacon minor(Integer minor) {
        this.minor = minor;
        return this;
    }

    public void setMinor(Integer minor) {
        this.minor = minor;
    }

    public String getMac() {
        return mac;
    }

    public RBeacon mac(String mac) {
        this.mac = mac;
        return this;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public RBeacon createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Category getCategory() {
        return category;
    }

    public RBeacon category(Category category) {
        this.category = category;
        return this;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Boolean isLost() {
        return lost;
    }

    public RBeacon lost(Boolean lost) {
        this.lost = lost;
        return this;
    }

    public void setLost(Boolean lost) {
        this.lost = lost;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RBeacon rBeacon = (RBeacon) o;
        if (rBeacon.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rBeacon.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RBeacon{" +
            "id=" + getId() +
            ", owner='" + getOwner() + "'" +
            ", tagName='" + getTagName() + "'" +
            ", identifier='" + getIdentifier() + "'" +
            ", major=" + getMajor() +
            ", minor=" + getMinor() +
            ", mac='" + getMac() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", category='" + getCategory() + "'" +
            ", lost='" + isLost() + "'" +
            "}";
    }
}
