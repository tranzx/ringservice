package mars.ring.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A TrackingEvent.
 */
@Entity
@Table(name = "tracking_event")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TrackingEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "owner", nullable = false)
    private String owner;

    @NotNull
    @Column(name = "mac", nullable = false)
    private String mac;

    @NotNull
    @Column(name = "record_time", nullable = false)
    private ZonedDateTime recordTime;

    @NotNull
    @Column(name = "received_time", nullable = false)
    private ZonedDateTime receivedTime;

    @NotNull
    @Column(name = "latitude", nullable = false)
    private Double latitude;

    @NotNull
    @Column(name = "longitude", nullable = false)
    private Double longitude;

    @Column(name = "distance")
    private Double distance;

    @ManyToOne
    private RBeacon rBeacon;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public TrackingEvent owner(String owner) {
        this.owner = owner;
        return this;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getMac() {
        return mac;
    }

    public TrackingEvent mac(String mac) {
        this.mac = mac;
        return this;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public ZonedDateTime getRecordTime() {
        return recordTime;
    }

    public TrackingEvent recordTime(ZonedDateTime recordTime) {
        this.recordTime = recordTime;
        return this;
    }

    public void setRecordTime(ZonedDateTime recordTime) {
        this.recordTime = recordTime;
    }

    public ZonedDateTime getReceivedTime() {
        return receivedTime;
    }

    public TrackingEvent receivedTime(ZonedDateTime receivedTime) {
        this.receivedTime = receivedTime;
        return this;
    }

    public void setReceivedTime(ZonedDateTime receivedTime) {
        this.receivedTime = receivedTime;
    }

    public Double getLatitude() {
        return latitude;
    }

    public TrackingEvent latitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public TrackingEvent longitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getDistance() {
        return distance;
    }

    public TrackingEvent distance(Double distance) {
        this.distance = distance;
        return this;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public RBeacon getRBeacon() {
        return rBeacon;
    }

    public TrackingEvent rBeacon(RBeacon RBeacon) {
        this.rBeacon = RBeacon;
        return this;
    }

    public void setRBeacon(RBeacon RBeacon) {
        this.rBeacon = RBeacon;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TrackingEvent trackingEvent = (TrackingEvent) o;
        if (trackingEvent.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), trackingEvent.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TrackingEvent{" +
            "id=" + getId() +
            ", owner='" + getOwner() + "'" +
            ", mac='" + getMac() + "'" +
            ", recordTime='" + getRecordTime() + "'" +
            ", receivedTime='" + getReceivedTime() + "'" +
            ", latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            ", distance=" + getDistance() +
            "}";
    }
}
