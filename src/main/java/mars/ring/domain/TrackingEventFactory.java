package mars.ring.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import mars.ring.application.util.DateUtil;
import mars.ring.repository.FoundNotificationRepository;
import mars.ring.repository.RBeaconRepository;
import mars.ring.repository.TrackingEventRepository;
import mars.ring.service.dto.RBeaconLTCommand;

/**
 * Creates tracking events.
 */
@Service
public class TrackingEventFactory {

    private final TrackingEventRepository trackingEventRepository;
    private final RBeaconRepository rBeaconRepository;
    private final FoundNotificationRepository foundNotificationRepository;

    public TrackingEventFactory(RBeaconRepository rBeaconRepository,
                                TrackingEventRepository trackingEventRepository,
                                FoundNotificationRepository foundNotificationRepository) {
        this.rBeaconRepository = rBeaconRepository;
        this.trackingEventRepository = trackingEventRepository;
        this.foundNotificationRepository = foundNotificationRepository;
    }

    public List<TrackingEvent> createTrackingEvents(Set<RBeaconLTCommand> traces, String reporter) {
        List<TrackingEvent> result = new ArrayList<TrackingEvent>();
        for (RBeaconLTCommand trace: traces) {
            if (trace.isValid()) {
                BluetoothAddress beaconId = new BluetoothAddress(trace.getMac());
                RBeacon foundBeacon = findRBeacon(beaconId);
                if (foundBeacon != null) {
                    TrackingEvent aEvent = new TrackingEvent();
                    aEvent.setRBeacon(foundBeacon);
                    aEvent.setOwner(foundBeacon.getOwner());
                    aEvent.setMac(beaconId.idString());
                    aEvent.setLatitude(trace.getLat());
                    aEvent.setLongitude(trace.getLon());
                    aEvent.setDistance(trace.getDistance());
                    aEvent.setRecordTime(trace.getRecordTime());
                    aEvent.setReceivedTime(DateUtil.nowUTC());
                    result.add(aEvent);
                    if (foundBeacon.isMissing()) { // Create a found notification value object
                        log.debug("Great a missing eTag founded!");
                        FoundNotification notification = new FoundNotification(aEvent);
                        foundNotificationRepository.save(notification);
                        if (foundBeacon.getOwner().equals(reporter)) { // handling auto-founded
                            foundBeacon.setLost(false);
                            rBeaconRepository.save(foundBeacon);
                        }
                    }
                    // remove old events
                    trackingEventRepository.deleteByMac(beaconId.idString());
                }
            }
        }
        return result;
    }

    private RBeacon findRBeacon(BluetoothAddress beaconId) {
        return rBeaconRepository.findByMac(beaconId.idString());
    }

    private final Logger log = LoggerFactory.getLogger(getClass());

}
