package mars.ring.domain.easypublish;

public enum PackageType {

    ANDROID, IOS, OTHERS;
}
