package mars.ring.service.dto;

import java.io.Serializable;
import mars.ring.domain.enumeration.Category;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the RBeacon entity. This class is used in RBeaconResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /r-beacons?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RBeaconCriteria implements Serializable {
    /**
     * Class for filtering Category
     */
    public static class CategoryFilter extends Filter<Category> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter owner;

    private StringFilter tagName;

    private StringFilter identifier;

    private IntegerFilter major;

    private IntegerFilter minor;

    private StringFilter mac;

    private ZonedDateTimeFilter createdAt;

    private CategoryFilter category;

    private BooleanFilter lost;

    public RBeaconCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getOwner() {
        return owner;
    }

    public void setOwner(StringFilter owner) {
        this.owner = owner;
    }

    public StringFilter getTagName() {
        return tagName;
    }

    public void setTagName(StringFilter tagName) {
        this.tagName = tagName;
    }

    public StringFilter getIdentifier() {
        return identifier;
    }

    public void setIdentifier(StringFilter identifier) {
        this.identifier = identifier;
    }

    public IntegerFilter getMajor() {
        return major;
    }

    public void setMajor(IntegerFilter major) {
        this.major = major;
    }

    public IntegerFilter getMinor() {
        return minor;
    }

    public void setMinor(IntegerFilter minor) {
        this.minor = minor;
    }

    public StringFilter getMac() {
        return mac;
    }

    public void setMac(StringFilter mac) {
        this.mac = mac;
    }

    public ZonedDateTimeFilter getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTimeFilter createdAt) {
        this.createdAt = createdAt;
    }

    public CategoryFilter getCategory() {
        return category;
    }

    public void setCategory(CategoryFilter category) {
        this.category = category;
    }

    public BooleanFilter getLost() {
        return lost;
    }

    public void setLost(BooleanFilter lost) {
        this.lost = lost;
    }

    @Override
    public String toString() {
        return "RBeaconCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (owner != null ? "owner=" + owner + ", " : "") +
                (tagName != null ? "tagName=" + tagName + ", " : "") +
                (identifier != null ? "identifier=" + identifier + ", " : "") +
                (major != null ? "major=" + major + ", " : "") +
                (minor != null ? "minor=" + minor + ", " : "") +
                (mac != null ? "mac=" + mac + ", " : "") +
                (createdAt != null ? "createdAt=" + createdAt + ", " : "") +
                (category != null ? "category=" + category + ", " : "") +
                (lost != null ? "lost=" + lost + ", " : "") +
            "}";
    }

}
