package mars.ring.service.dto;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import mars.ring.domain.FoundNotification;
import mars.ring.domain.enumeration.Category;

public class FoundNotificationDTO {

    final Double lat;
    final Double lon;
    final Double distance;
    final ZonedDateTime recordedAt;
    final ZonedDateTime receivedAt;
    final ZonedDateTime createdAt;
    final String tagName;
    final Category category;

    public FoundNotificationDTO(FoundNotification fn) {
        lat = fn.getLat();
        lon = fn.getLon();
        distance = fn.getDistance();
        recordedAt = fn.getRecordedAt();
        receivedAt = fn.getReceivedAt();
        createdAt = fn.getCreatedAt();
        tagName = fn.getRBeacon().getTagName();
        category = fn.getRBeacon().getCategory();
    }

    public static List<FoundNotificationDTO> toDTO(List<FoundNotification> list) {
        List<FoundNotificationDTO> result = new ArrayList<FoundNotificationDTO>();
        for (FoundNotification fn: list) {
            result.add(new FoundNotificationDTO(fn));
        }
        return result;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public Double getDistance() {
        return distance;
    }

    public ZonedDateTime getRecordedAt() {
        return recordedAt;
    }

    public ZonedDateTime getReceivedAt() {
        return receivedAt;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public String getTagName() {
        return tagName;
    }

    public Category getCategory() {
        return category;
    }

}