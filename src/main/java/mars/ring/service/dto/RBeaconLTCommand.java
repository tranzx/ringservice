package mars.ring.service.dto;

import java.time.ZonedDateTime;

import org.springframework.util.StringUtils;

/**
 * A RBeacon location time event.
 */
public class RBeaconLTCommand {

    private String mac;
    private ZonedDateTime recordTime;
    private Double lat;
    private Double lon;
    private Double distance;

    public boolean isValid() {
        return isNotEmpty(mac) && isNotEmpty(recordTime) && isNotEmpty(lat) && isNotEmpty(lon);
    }

    private boolean isNotEmpty(Object str) {
        return ! StringUtils.isEmpty(str);
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public ZonedDateTime getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(ZonedDateTime recordTime) {
        this.recordTime = recordTime;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

}