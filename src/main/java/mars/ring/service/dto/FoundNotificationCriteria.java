package mars.ring.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the FoundNotification entity. This class is used in FoundNotificationResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /found-notifications?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class FoundNotificationCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter owner;

    private DoubleFilter lat;

    private DoubleFilter lon;

    private DoubleFilter distance;

    private ZonedDateTimeFilter recordedAt;

    private ZonedDateTimeFilter receivedAt;

    private ZonedDateTimeFilter createdAt;

    private LongFilter rBeaconId;

    public FoundNotificationCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getOwner() {
        return owner;
    }

    public void setOwner(StringFilter owner) {
        this.owner = owner;
    }

    public DoubleFilter getLat() {
        return lat;
    }

    public void setLat(DoubleFilter lat) {
        this.lat = lat;
    }

    public DoubleFilter getLon() {
        return lon;
    }

    public void setLon(DoubleFilter lon) {
        this.lon = lon;
    }

    public DoubleFilter getDistance() {
        return distance;
    }

    public void setDistance(DoubleFilter distance) {
        this.distance = distance;
    }

    public ZonedDateTimeFilter getRecordedAt() {
        return recordedAt;
    }

    public void setRecordedAt(ZonedDateTimeFilter recordedAt) {
        this.recordedAt = recordedAt;
    }

    public ZonedDateTimeFilter getReceivedAt() {
        return receivedAt;
    }

    public void setReceivedAt(ZonedDateTimeFilter receivedAt) {
        this.receivedAt = receivedAt;
    }

    public ZonedDateTimeFilter getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTimeFilter createdAt) {
        this.createdAt = createdAt;
    }

    public LongFilter getRBeaconId() {
        return rBeaconId;
    }

    public void setRBeaconId(LongFilter rBeaconId) {
        this.rBeaconId = rBeaconId;
    }

    @Override
    public String toString() {
        return "FoundNotificationCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (owner != null ? "owner=" + owner + ", " : "") +
                (lat != null ? "lat=" + lat + ", " : "") +
                (lon != null ? "lon=" + lon + ", " : "") +
                (distance != null ? "distance=" + distance + ", " : "") +
                (recordedAt != null ? "recordedAt=" + recordedAt + ", " : "") +
                (receivedAt != null ? "receivedAt=" + receivedAt + ", " : "") +
                (createdAt != null ? "createdAt=" + createdAt + ", " : "") +
                (rBeaconId != null ? "rBeaconId=" + rBeaconId + ", " : "") +
            "}";
    }

}
