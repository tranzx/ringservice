package mars.ring.service.dto;

import java.util.ArrayList;
import java.util.List;

import mars.ring.domain.RBeacon;
import mars.ring.domain.enumeration.Category;

public class RBeaconDTO {

    Long id;
    String tagName;
    Category category;
    String mac;
    String identifier;
    Integer major;
    Integer minor;
    Boolean lost;

    RBeaconDTO() {}

    public RBeaconDTO(RBeacon rBeacon) {
        id = rBeacon.getId();
        tagName = rBeacon.getTagName();
        category = rBeacon.getCategory();
        mac = rBeacon.getMac();
        identifier = rBeacon.getIdentifier();
        major = rBeacon.getMajor();
        minor = rBeacon.getMinor();
        lost = rBeacon.isLost();
    }

    public boolean isMissing() {
        return lost != null && lost;
    }

    public Long getId() {
        return id;
    }

    public String getTagName() {
        return tagName;
    }

    public String getMac() {
        return mac;
    }

    public String getIdentifier() {
        return identifier;
    }

    public Integer getMajor() {
        return major;
    }

    public Integer getMinor() {
        return minor;
    }

    public Category getCategory() {
        return category;
    }

    public Boolean getLost() {
        return lost;
    }

    public static List<RBeaconDTO> toDTOList(List<RBeacon> list) {
        List<RBeaconDTO> result = new ArrayList<RBeaconDTO>(list.size());
        for (RBeacon rBeacon: list) {
            result.add(new RBeaconDTO(rBeacon));
        }
        return result;
    }

    public RBeacon toModel() {
        RBeacon rBeacon = new RBeacon();
        rBeacon.setId(id);
        rBeacon.setTagName(tagName);
        rBeacon.setCategory(category);
        rBeacon.setMac(mac);
        rBeacon.setIdentifier(identifier);
        rBeacon.setMajor(major);
        rBeacon.setMinor(minor);
        rBeacon.setLost(lost);
        return rBeacon;
    }

    @Override
    public int hashCode() {
        return mac.hashCode();
    }

    @Override
    public String toString() {
        return tagName;
    }

}