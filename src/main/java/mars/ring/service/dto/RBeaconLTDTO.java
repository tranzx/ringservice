package mars.ring.service.dto;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import mars.ring.domain.TrackingEvent;
import mars.ring.domain.enumeration.Category;

/**
 * A RBeacon location time event.
 */
/**
 * @author developer
 *
 */
public class RBeaconLTDTO {

    private String mac;
    private Double lat;
    private Double lon;
    private Double distance;
    private ZonedDateTime recordTime;
    private ZonedDateTime receivedTime;
    private String tagName;
    private Category category;

    public RBeaconLTDTO() {}

    public RBeaconLTDTO(TrackingEvent e) {
        this.mac = e.getMac();
        this.lat = e.getLatitude();
        this.lon = e.getLongitude();
        this.distance = e.getDistance();
        this.recordTime = e.getRecordTime();
        this.receivedTime = e.getReceivedTime();
        this.tagName = e.getRBeacon().getTagName();
        this.category = e.getRBeacon().getCategory();
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public ZonedDateTime getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(ZonedDateTime recordTime) {
        this.recordTime = recordTime;
    }

    public ZonedDateTime getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(ZonedDateTime receivedTime) {
        this.receivedTime = receivedTime;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public static List<RBeaconLTDTO> fromModel(List<TrackingEvent> list) {
        List<RBeaconLTDTO> result = new ArrayList<RBeaconLTDTO>();
        for (TrackingEvent e: list) {
            result.add(new RBeaconLTDTO(e));
        }
        return result;
    }

}