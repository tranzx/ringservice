package mars.ring.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mars.ring.domain.RBeacon;
import mars.ring.repository.FoundNotificationRepository;
import mars.ring.repository.RBeaconRepository;
import mars.ring.repository.TrackingEventRepository;


/**
 * Service Implementation for managing RBeacon.
 */
@Service
@Transactional
public class RBeaconService {

    private final Logger log = LoggerFactory.getLogger(RBeaconService.class);

    private final RBeaconRepository rBeaconRepository;
    private final TrackingEventRepository trackingEventRepository;
    private final FoundNotificationRepository foundNotificationRepository;

    public RBeaconService(RBeaconRepository rBeaconRepository,
            TrackingEventRepository trackingEventRepository,
            FoundNotificationRepository foundNotificationRepository) {
        this.rBeaconRepository = rBeaconRepository;
        this.trackingEventRepository = trackingEventRepository;
        this.foundNotificationRepository = foundNotificationRepository;
    }

    public boolean isRegisteredBefore(RBeacon rBeacon) {
        RBeacon found = rBeaconRepository.findByMac(rBeacon.getMac());
        return found != null;
    }

    /**
     * Save a rBeacon.
     *
     * @param rBeacon the entity to save
     * @return the persisted entity
     */
    public RBeacon save(RBeacon rBeacon) {
        log.debug("Request to save RBeacon : {}", rBeacon);
        return rBeaconRepository.save(rBeacon);
    }

    /**
     * Get all the rBeacons.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RBeacon> findAll(Pageable pageable) {
        log.debug("Request to get all RBeacons");
        return rBeaconRepository.findAll(pageable);
    }

    /**
     * Get one rBeacon by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public RBeacon findOne(Long id) {
        log.debug("Request to get RBeacon : {}", id);
        return rBeaconRepository.findOne(id);
    }

    /**
     * Delete the rBeacon by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete RBeacon : {}", id);
        RBeacon forDelete = findOne(id);
        trackingEventRepository.deleteByRBeacon(forDelete);
        foundNotificationRepository.deleteByRBeacon(forDelete);
        rBeaconRepository.delete(id);
    }

}