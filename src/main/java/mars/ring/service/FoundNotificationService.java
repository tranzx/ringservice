package mars.ring.service;

import mars.ring.domain.FoundNotification;
import mars.ring.repository.FoundNotificationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing FoundNotification.
 */
@Service
@Transactional
public class FoundNotificationService {

    private final Logger log = LoggerFactory.getLogger(FoundNotificationService.class);

    private final FoundNotificationRepository foundNotificationRepository;

    public FoundNotificationService(FoundNotificationRepository foundNotificationRepository) {
        this.foundNotificationRepository = foundNotificationRepository;
    }

    /**
     * Save a foundNotification.
     *
     * @param foundNotification the entity to save
     * @return the persisted entity
     */
    public FoundNotification save(FoundNotification foundNotification) {
        log.debug("Request to save FoundNotification : {}", foundNotification);
        return foundNotificationRepository.save(foundNotification);
    }

    /**
     * Get all the foundNotifications.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FoundNotification> findAll(Pageable pageable) {
        log.debug("Request to get all FoundNotifications");
        return foundNotificationRepository.findAll(pageable);
    }

    /**
     * Get one foundNotification by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public FoundNotification findOne(Long id) {
        log.debug("Request to get FoundNotification : {}", id);
        return foundNotificationRepository.findOne(id);
    }

    /**
     * Delete the foundNotification by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete FoundNotification : {}", id);
        foundNotificationRepository.delete(id);
    }
}
