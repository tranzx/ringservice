package mars.ring.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mars.ring.domain.FoundNotification;
import mars.ring.domain.*; // for static metamodels
import mars.ring.repository.FoundNotificationRepository;
import mars.ring.service.dto.FoundNotificationCriteria;


/**
 * Service for executing complex queries for FoundNotification entities in the database.
 * The main input is a {@link FoundNotificationCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FoundNotification} or a {@link Page} of {@link FoundNotification} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FoundNotificationQueryService extends QueryService<FoundNotification> {

    private final Logger log = LoggerFactory.getLogger(FoundNotificationQueryService.class);


    private final FoundNotificationRepository foundNotificationRepository;

    public FoundNotificationQueryService(FoundNotificationRepository foundNotificationRepository) {
        this.foundNotificationRepository = foundNotificationRepository;
    }

    /**
     * Return a {@link List} of {@link FoundNotification} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FoundNotification> findByCriteria(FoundNotificationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<FoundNotification> specification = createSpecification(criteria);
        return foundNotificationRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link FoundNotification} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FoundNotification> findByCriteria(FoundNotificationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<FoundNotification> specification = createSpecification(criteria);
        return foundNotificationRepository.findAll(specification, page);
    }

    /**
     * Function to convert FoundNotificationCriteria to a {@link Specifications}
     */
    private Specifications<FoundNotification> createSpecification(FoundNotificationCriteria criteria) {
        Specifications<FoundNotification> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), FoundNotification_.id));
            }
            if (criteria.getOwner() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOwner(), FoundNotification_.owner));
            }
            if (criteria.getLat() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLat(), FoundNotification_.lat));
            }
            if (criteria.getLon() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLon(), FoundNotification_.lon));
            }
            if (criteria.getDistance() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDistance(), FoundNotification_.distance));
            }
            if (criteria.getRecordedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRecordedAt(), FoundNotification_.recordedAt));
            }
            if (criteria.getReceivedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getReceivedAt(), FoundNotification_.receivedAt));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), FoundNotification_.createdAt));
            }
            if (criteria.getRBeaconId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getRBeaconId(), FoundNotification_.rBeacon, RBeacon_.id));
            }
        }
        return specification;
    }

}
