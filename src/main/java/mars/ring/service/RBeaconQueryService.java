package mars.ring.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mars.ring.domain.RBeacon;
import mars.ring.domain.*; // for static metamodels
import mars.ring.repository.RBeaconRepository;
import mars.ring.service.dto.RBeaconCriteria;

import mars.ring.domain.enumeration.Category;

/**
 * Service for executing complex queries for RBeacon entities in the database.
 * The main input is a {@link RBeaconCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RBeacon} or a {@link Page} of {@link RBeacon} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RBeaconQueryService extends QueryService<RBeacon> {

    private final Logger log = LoggerFactory.getLogger(RBeaconQueryService.class);


    private final RBeaconRepository rBeaconRepository;

    public RBeaconQueryService(RBeaconRepository rBeaconRepository) {
        this.rBeaconRepository = rBeaconRepository;
    }

    /**
     * Return a {@link List} of {@link RBeacon} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RBeacon> findByCriteria(RBeaconCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<RBeacon> specification = createSpecification(criteria);
        return rBeaconRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RBeacon} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RBeacon> findByCriteria(RBeaconCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<RBeacon> specification = createSpecification(criteria);
        return rBeaconRepository.findAll(specification, page);
    }

    /**
     * Function to convert RBeaconCriteria to a {@link Specifications}
     */
    private Specifications<RBeacon> createSpecification(RBeaconCriteria criteria) {
        Specifications<RBeacon> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RBeacon_.id));
            }
            if (criteria.getOwner() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOwner(), RBeacon_.owner));
            }
            if (criteria.getTagName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTagName(), RBeacon_.tagName));
            }
            if (criteria.getIdentifier() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIdentifier(), RBeacon_.identifier));
            }
            if (criteria.getMajor() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMajor(), RBeacon_.major));
            }
            if (criteria.getMinor() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMinor(), RBeacon_.minor));
            }
            if (criteria.getMac() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMac(), RBeacon_.mac));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), RBeacon_.createdAt));
            }
            if (criteria.getCategory() != null) {
                specification = specification.and(buildSpecification(criteria.getCategory(), RBeacon_.category));
            }
            if (criteria.getLost() != null) {
                specification = specification.and(buildSpecification(criteria.getLost(), RBeacon_.lost));
            }
        }
        return specification;
    }

}
